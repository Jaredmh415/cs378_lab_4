// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Lab4PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CS378_LAB4_API ALab4PlayerController : public APlayerController
{
	GENERATED_BODY()
    
public:
    ALab4PlayerController();
    
    UFUNCTION()
    void JumpAction();
    UFUNCTION()
    void InteractAction();
    UFUNCTION()
    void ForwardMove(float value);
    UFUNCTION()
    void RightMove(float value);
   
    
    
protected:
    
    virtual void SetupInputComponent() override;
	
};
