// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TimerManager.h"
#include "Lab4Character.generated.h"

UENUM(BlueprintType)
enum class ECharacterActionStateEnum: uint8 {
    IDLE UMETA(DisplayName = "Idling"),
    MOVE UMETA(DisplayName = "Moving"),
    JUMP UMETA(DisplayName = "Jumping"),
    INTERACT UMETA(DisplayName = "Interacting")
};


UCLASS()
class CS378_LAB4_API ALab4Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ALab4Character();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    
    FTimerHandle InteractionTimerHandle;
    
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    ECharacterActionStateEnum CharacterActionState;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    
    UFUNCTION(BlueprintImplementableEvent)
    void InteractPressed();
    
    UFUNCTION(BlueprintImplementableEvent)
    void JumpMovementPressed();
    
    UFUNCTION(BlueprintImplementableEvent)
    void ForwardMovementAxis(float value);
    
    UFUNCTION(BlueprintImplementableEvent)
    void RightMovementAxis(float value);
    
    UFUNCTION(BlueprintCallable)
    void MoveForward(float value);
    
    UFUNCTION(BlueprintCallable)
    void MoveRight(float value);
    
    UFUNCTION(BlueprintCallable)
    void JumpUp();
    
    UFUNCTION(BlueprintCallable)
    void Interact();
    
    UFUNCTION(BlueprintCallable)
    bool CanPerformAction(ECharacterActionStateEnum updatedAction);
    
    UFUNCTION(BlueprintCallable)
    void BeginInteraction();
    
    UFUNCTION(BlueprintCallable)
    void EndInteraction();
    
    UFUNCTION(BlueprintCallable)
    void UpdateActionState(ECharacterActionStateEnum newAction);

    // Static names for axis bindings
    static const FName MoveForwardBinding;
    static const FName MoveRightBinding;
    
    //Static names for action bindings
    static const FName JumpBinding;
    static const FName InteractBinding;

};
