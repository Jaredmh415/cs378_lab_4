// Copyright Epic Games, Inc. All Rights Reserved.

#include "Lab4Character.h"
#include "Lab4PlayerController.h"
#include "CS378_Lab4GameModeBase.h"
ACS378_Lab4GameModeBase::ACS378_Lab4GameModeBase()
{
    static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Blueprint'/Game/Blueprints/Lab4CharacterBP.Lab4CharacterBP_C'"));
     if (pawnBPClass.Object)
    {
        if (GEngine)
        {
         GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Found"));
        }
        UClass* pawnBP = (UClass* )pawnBPClass.Object;
        DefaultPawnClass = pawnBP;
    }
    else
    {
        GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Not Found"));
        DefaultPawnClass = ALab4Character::StaticClass();
    }
    PlayerControllerClass = ALab4PlayerController::StaticClass();
    
    
}
