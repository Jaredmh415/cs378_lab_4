// Fill out your copyright notice in the Description page of Project Settings.


#include "TimerManager.h"
#include "Lab4Character.h"

// Sets default values
ALab4Character::ALab4Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALab4Character::BeginPlay()
{
	Super::BeginPlay();
    
    
	
}

// Called every frame
void ALab4Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ALab4Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ALab4Character::MoveForward(float value)
{
    FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
    AddMovementInput(Direction, value);
}
void ALab4Character::MoveRight(float value)
{
    FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
    AddMovementInput(Direction, value);
}
void ALab4Character::JumpUp()
{
    this->Jump();
}
void ALab4Character::Interact()
{
    
}
bool ALab4Character::CanPerformAction(ECharacterActionStateEnum updatedAction)
{
    switch(CharacterActionState)
    {
        case ECharacterActionStateEnum::IDLE:
            return true;
            break;
        case ECharacterActionStateEnum::MOVE:
            if(updatedAction != ECharacterActionStateEnum::INTERACT)
            {
                return true;
            }
            break;
        case ECharacterActionStateEnum::JUMP:
            if (updatedAction == ECharacterActionStateEnum::IDLE || updatedAction == ECharacterActionStateEnum::MOVE)
            {
                return true;
            }
            break;
        case ECharacterActionStateEnum::INTERACT:
            return false;
    }
    return false;
}
void ALab4Character::BeginInteraction()
{
    if(GEngine)
    {
        GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Begin Interaction"));
    }
    GetWorld()->GetTimerManager().SetTimer(InteractionTimerHandle, this, &ALab4Character::EndInteraction, 3.0f, false);
    
    
}
void ALab4Character::EndInteraction()
{
    if(GEngine)
    {
        GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("End Interaction"));
    }
    UpdateActionState(ECharacterActionStateEnum::IDLE);
    
}
void ALab4Character::UpdateActionState(ECharacterActionStateEnum newAction)
{
    if (newAction == ECharacterActionStateEnum::MOVE || newAction == ECharacterActionStateEnum::IDLE)
    {
        if (FMath::Abs(GetVelocity().Size()) <= 0.01f)
        {
            CharacterActionState = ECharacterActionStateEnum::IDLE;
        }
        else
        {
            CharacterActionState = ECharacterActionStateEnum::MOVE;
            
        }
    }
    else
    {
        CharacterActionState = newAction;
    }
}
