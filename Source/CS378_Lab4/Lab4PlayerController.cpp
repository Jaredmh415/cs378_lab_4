// Fill out your copyright notice in the Description page of Project Settings.

#include "Lab4PlayerController.h"
#include "Lab4Character.h"

ALab4PlayerController::ALab4PlayerController()
{
    
}

void ALab4PlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();
    InputComponent->BindAction("JumpMovement", IE_Pressed, this, &ALab4PlayerController::JumpAction);
    InputComponent->BindAction("InteractionKey", IE_Pressed, this, &ALab4PlayerController::InteractAction);
    InputComponent->BindAxis("ForwardMovement", this, &ALab4PlayerController::ForwardMove);
    InputComponent->BindAxis("RightMovement", this, &ALab4PlayerController::RightMove);
    
    
}
void ALab4PlayerController::InteractAction()
{
    ALab4Character * character = Cast<ALab4Character>(this->GetCharacter());
    if (character)
    {
        character->InteractPressed();
    }
}
void ALab4PlayerController::ForwardMove(float value)
{
//    if (GEngine)
//    {
//     GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Black, FString::Printf(TEXT("Forward Controller: %f"), value));
//    }
    ALab4Character * character = Cast<ALab4Character>(this->GetCharacter());
    if (character)
    {
        character->ForwardMovementAxis(value);
    }
}
void ALab4PlayerController::RightMove(float value)
{
    ALab4Character * character = Cast<ALab4Character>(this->GetCharacter());
    if (character)
    {
        character->RightMovementAxis(value);
    }
}
void ALab4PlayerController::JumpAction()
{
    ALab4Character * character = Cast<ALab4Character>(this->GetCharacter());
    if (character)
    {
        character->JumpMovementPressed();
    }
}
